from dataclasses import field
from dataclasses import dataclass

from cleandev_validator import DataClass, _DataClassConstrains

@dataclass()
class Animal(DataClass):
    peso: int
    edad: int
    active: bool
    nombre: str = field(init=False)

    def __post_init__(self):
        super(Animal, self)._validate(**self.__dict__)

    @property
    def __constrains__(self):
        return {
            'peso': str(_DataClassConstrains.INT),
            'edad': str(_DataClassConstrains.INT),
            'active': str(_DataClassConstrains.BOOL),
        }


if __name__ == '__main__':
    animal: Animal = Animal(peso=20, edad=20, active=True)
    animal.__filter__(['peso'])  # {'peso': 20}
    animal.__filter__(['peso'], False)  # {'edad': 'tengo 10 años'}
