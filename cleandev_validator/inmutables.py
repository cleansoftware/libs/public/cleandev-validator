from backports.strenum import StrEnum

class _DataClassConstrains(StrEnum):
    BOOL = 'bool'
    STR = 'str'
    INT = 'int'

